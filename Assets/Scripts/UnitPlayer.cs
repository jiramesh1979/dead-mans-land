﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPlayer : MonoBehaviour
{
    public float checkRate = 1.0f;
    public float nearbyEnemyAttackRange = 30f;

    public LayerMask unitLayerMask;

    private Unit _unit;

    void Awake()
    {
        _unit = GetComponent<Unit>();
    }

    void Start()
    {
        InvokeRepeating("Check", 0.0f, checkRate);
        unitLayerMask = LayerMask.GetMask("Unit");
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, nearbyEnemyAttackRange);
    }   

    void Check()
    {
        if (!_unit.isWorker)// if a unit is not a worker
        {
            // check if we have nearby enemies - if so, attack them
            if (_unit.state != UnitState.AttackUnit && _unit.state != UnitState.MoveToEnemy)
            {
                Unit potentialEnemy = CheckForNearbyEnemies();

                if (potentialEnemy != null)
                    _unit.AttackUnit(potentialEnemy);
            }
        }
    }

    // checks for nearby enemies with a sphere cast
    Unit CheckForNearbyEnemies()
    {
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, nearbyEnemyAttackRange, Vector3.up, unitLayerMask);

        GameObject closest = null;
        float closestDist = 0.0f;

        for (int x = 0; x < hits.Length; x++)
        {
            //Debug.Log("Test - " + hits[x].collider.gameObject.ToString());

            // skip if this is not a unit
            if (hits[x].collider.gameObject.GetComponent<Unit>() == null)
                continue;

            // skip if this is us
            if (hits[x].collider.gameObject == gameObject)
                continue;

            // is this a team mate?
            if (_unit.player.IsMyUnit(hits[x].collider.GetComponent<Unit>()))
            {
                continue;
            }
            // if there is not closest enemy or the distance is less than the closest distance it currently has
            else if (!closest || Vector3.Distance(transform.position, hits[x].transform.position) < closestDist)
            {
                // if there is not closest enemy or the distance is less than the closest distance it currently has
                if (!closest || Vector3.Distance(transform.position, hits[x].transform.position) < closestDist)
                {
                    closest = hits[x].collider.gameObject;
                    closestDist = Vector3.Distance(transform.position, hits[x].transform.position);
                }
            }
        }

        if (closest != null)
        {
            Debug.Log(closest.gameObject.ToString() + ", " + closestDist.ToString());
            return closest.GetComponent<Unit>();
        }
        else
            return null;
    }
}
