﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;


public class ActionManager : MonoBehaviour
{
    public static ActionManager instance;

    public Button[] buttonUnitPic;
    public Button[] buttonBuildingPic;

    private Building _curBuilding;

    private Unit _curUnit;

    public ActionManager()
    {
        instance = this;
    }

    public void ShowCreateUnitButtons(Building building)
    {
        ClearAllInfo();

        _curBuilding = building;

        if (_curBuilding.isFunctional)
        {
            for (int i = 0; i < _curBuilding.unitList.Length; i++)
            {
                buttonUnitPic[i].gameObject.SetActive(true);

                Unit unit = _curBuilding.unitList[i].GetComponent<Unit>();

                buttonUnitPic[i].image.sprite = unit.unitPic;
            }
        }
    }

    public void HideCreateUnitButtons()
    {
        for (int i = 0; i < buttonUnitPic.Length; i++)
        {
            buttonUnitPic[i].gameObject.SetActive(false);
        }
    }

    public void ShowCreateBuildingButtons(Unit unit)
    {
        ClearAllInfo();

        _curUnit = unit;

        if (_curUnit.isBuilder)
        {
            for (int i = 0; i < _curUnit.builder.buildingList.Length; i++)
            {
                buttonBuildingPic[i].gameObject.SetActive(true);

                Building building = _curUnit.builder.buildingList[i].GetComponent<Building>();

                buttonBuildingPic[i].image.sprite = building.buildingPic;
            }
        }
    }

    public void HideCreateBuildingButtons()
    {
        for (int i = 0; i < buttonBuildingPic.Length; i++)
        {
            buttonBuildingPic[i].gameObject.SetActive(false);
        }
    }

    public void ClearAllInfo()
    {
        HideCreateUnitButtons();
        HideCreateBuildingButtons();
    }

    public void CreateUnitButton(int n)
    {
        //Debug.Log("Create " + n);

        _curBuilding.ToCreateUnit(n);
    }

    public void CreateBuildingButton(int n)
    {
        //Debug.Log("Create Building " + n);

        _curUnit.builder.ToCreateNewBuilding(n);
    }
}
