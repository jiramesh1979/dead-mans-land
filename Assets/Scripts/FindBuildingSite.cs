﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindBuildingSite : MonoBehaviour
{
    [SerializeField]
    private bool _canBuild = false;

    public bool CanBuild { get { return _canBuild; } set { _canBuild = value; } }

    private void OnTriggerEnter(Collider other)
    {
        gameObject.GetComponent<Renderer>().material.color = Color.red;
        _canBuild = false;
    }

    private void OnTriggerExit(Collider other)
    {
        gameObject.GetComponent<Renderer>().material.color = Color.green;
        _canBuild = true;
    }
}
