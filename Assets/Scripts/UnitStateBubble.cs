﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnitStateBubble : MonoBehaviour
{
    public Image stateBubble;

    public Sprite idleState;
    public Sprite gatherState;
    public Sprite attackState;
    public Sprite toHQState;
    public Sprite moveState;

    public void OnStateChange(UnitState state)
    {
        stateBubble.enabled = true;

        switch(state)
        {
            case UnitState.Idle:
                {
                    stateBubble.sprite = idleState;
                    break;
                }
            case UnitState.Gather:
                {
                    stateBubble.sprite = gatherState;
                    break;
                }
            case UnitState.AttackUnit:
                {
                    stateBubble.sprite = attackState;
                    break;
                }
            case UnitState.AttackBuilding:
                {
                    stateBubble.sprite = attackState;
                    break;
                }
            case UnitState.MoveToHQ:
                {
                    stateBubble.sprite = toHQState;
                    break;
                }
            case UnitState.Move:
                {
                    stateBubble.sprite = moveState;
                    break;
                }
            default:
                {
                    stateBubble.enabled = false;
                    break;
                }
        }
    }
}
