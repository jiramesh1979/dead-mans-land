﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    [Header("Stats")]

    public string buildingName;
    public Sprite buildingPic;

    public bool isHQ;
    public bool isFunctional;

    [SerializeField]
    private int _curHP;

    public int maxHP;

    public int CurHP { get { return _curHP; } set { _curHP = value; } }

    public int cost;

    public float intoTheGround = 5f; // How deep into the ground the building is at the construction site.
    public float radiusFromCenter = 6f; //How far from the building's center to have a builder to begin his job.

    [Header("Components")]
    public Player player;
    public GameObject SelectionVisual; //Selection Ring

    public UnitHealthBar healthBar;

    public GameObject fire;
    public GameObject smoke;

    public Transform unitSpawnPos;
    public Transform unitRallyPos;

    public GameObject[] unitList; //Units that this building can recruit

    public List<GameObject> recruitList = new List<GameObject>();

    private float _timer = 0f;
    public float waitTime = 0.5f; //How fast the building is constructed. Lower is faster.

    public float Timer { get { return _timer; } set { _timer = value; } }

    private float _unitTimer = 0f;
    private float currentUnitWaitTime;

    private float _progress = 0f;

    public float Progress { get { return _progress; } set { _progress = value; } }

    void Start()
    {
        
    }

    void Update()
    {
        if (isFunctional && recruitList.Count > 0)
        {
            //Debug.Log(gameObject);

            _unitTimer += Time.deltaTime;

            //Debug.Log(timer);

            currentUnitWaitTime = recruitList[0].GetComponent<Unit>().GetUnitWaitTime();

            if (_unitTimer >= currentUnitWaitTime)
            {
                _unitTimer = 0f;
                IncreaseProgress();

                if (_progress >= 100f)
                {
                    if (player.units.Count < player.unitLimit)
                    {
                        _progress = 0f;
                        CreateUnitCompleted();
                    }
                }
            }
        }      
    }

    // toggles green selection ring around resource
    public void ToggleSelectionVisual(bool selected)
    {
        if (SelectionVisual != null)
            SelectionVisual.SetActive(selected);
    }

    public bool CheckCommanderLimit(int i)
    {
        Unit unit = unitList[i].GetComponent<Unit>();

        if (unit.isCommander)
        {
            if (player.currentCommanderCount >= player.commanderLimit)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }

    public bool CheckCommanderLimitFromList()
    {
        if (recruitList == null)
        {
            return false;
        }
        else
        {
            foreach(GameObject go in recruitList)
            {
                Unit u = go.GetComponent<Unit>();

                if (u.isCommander)
                {
                    return true;
                }
            }

            return false;
        }
    }

    public void ToCreateUnit(int i)
    {
        if(player.food - player.unitCost < 0)
        {
            return;
        }

        //Check commander Limit
        if (CheckCommanderLimit(i))
        {
            return;
        }

        //Check commander Limit from recruit list
        if (CheckCommanderLimitFromList())
        {
            return;
        }

        player.food -= player.unitCost;

        if (player.isMe)
        {
            GameUI.instance.UpdateUnitCountText(player.units.Count, player.unitLimit);
            GameUI.instance.UpdateFoodText(player.food);
        }

        recruitList.Add(unitList[i]);
    }

    void IncreaseProgress()
    {
        _progress++;
    }

    public void CreateUnitCompleted()
    {
        GameObject unitObj = Instantiate(recruitList[0], unitSpawnPos.position, transform.rotation, transform);

        unitObj.transform.parent = player.unitsParent.transform;
        
        unitObj.AddComponent<UnitPlayer>();
 
        Unit unit = unitObj.GetComponent<Unit>();

        player.units.Add(unit);
        unit.player = this.player; //set a unit's faction to be belong to this player

        if (unit.isCommander)
        {
            unit.player.currentCommanderCount++;
        }

        recruitList.RemoveAt(0);

        if (player.isMe)
        {
            GameUI.instance.UpdateUnitCountText(player.units.Count, player.unitLimit);
        }

        if (recruitList.Count == 0)
        {
            InfoManager.instance.HideProgressMode();
        }
        else
        {
            InfoManager.instance.ClearRecruitList();
        }

        unit.MoveToPosition(unitRallyPos.position);
    }

    // called when an enemy unit attacks us
    public void TakeDamage(int damage)
    {
        _curHP -= damage;

        if (_curHP <= 0)
        {
            _curHP = 0;
            Die();
        }

        fire.SetActive(true);
        smoke.SetActive(true);

        // update the health bar
        //healthBar.UpdateHealthBar(curHP, maxHP);
    }

    // called when our health reaches zero
    void Die()
    {
        player.buildings.Remove(this);

        InfoManager.instance.ClearAllInfo();

        GameManager.instance.UnitDeathCheck();

        Destroy(gameObject);
    }
}
