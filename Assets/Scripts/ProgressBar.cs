﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    public GameObject ProgressContainer;
    public RectTransform Fill;

    public Slider slider;

    void Start()
    {
        slider.minValue = 0;
        slider.maxValue = 100;
    }

    public void UpdateProgressBar(float progress)
    {
        ProgressContainer.SetActive(true);

        slider.value = progress;
    }

    public void ResetProgressBar()
    {
        slider.value = slider.minValue;
    }
}
