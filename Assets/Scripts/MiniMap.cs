﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMap : MonoBehaviour
{
    public RectTransform viewPort;
    public Transform Corner1, Corner2;

    private Vector2 _worldSize;

    private RectTransform _miniMapRect;

    public GameObject miniMapCam;

    public GameObject blipPrefab;

    public static MiniMap current;

    public MiniMap()
    {
        current = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        _worldSize = new Vector2(Corner2.transform.position.x - Corner1.transform.position.x,
            Corner2.transform.position.z - Corner1.transform.position.z);

        //Debug.Log("World size is " + worldSize);

        _miniMapRect = GetComponent<RectTransform>();

        //Debug.Log("MiniMap is " + miniMapRect.rect.width + ", " + miniMapRect.rect.height);
    }

    public Vector2 worldToMinimapPos(Vector3 worldPos)
    {
        //var pos = worldPos - Corner1.position;

        Vector2 minimapPos = new Vector2((worldPos.x / _worldSize.x * _miniMapRect.rect.width) + _worldSize.x/2,
            (worldPos.z / _worldSize.y * _miniMapRect.rect.height) + _worldSize.y/2);

        
        
        return minimapPos;
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Camera Pos " + miniMapCam.transform.position);

        viewPort.position = worldToMinimapPos(new Vector3(miniMapCam.transform.position.x, miniMapCam.transform.position.y, miniMapCam.transform.position.z));

        //Debug.Log("View Port " + viewPort.position);
    }
}
