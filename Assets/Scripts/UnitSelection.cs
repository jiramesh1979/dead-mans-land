﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnitSelection : MonoBehaviour
{
    public RectTransform selectionBox;
    public LayerMask unitLayerMask;
    public LayerMask resourceLayerMask;
    public LayerMask buildingLayerMask;

    private List<Unit> _selectedUnits =  new List<Unit>(); // list of friendly units selected

    private Unit _curUnit = null; // current selected single friendly unit
    private Unit _curEnemy = null; // current selected enemy
    private ResourceSource _curResource = null; // current selected enemy
    private Building _curBuilding = null; // current selected building

    private Vector2 _startPos;

    private bool _selected = false; // is there something selected?

    //components
    private Camera _cam;
    private Player _player;

    void Awake()
    {
        // get the components
        _cam = Camera.main;
        _player = GetComponent<Player>();
        unitLayerMask = LayerMask.GetMask("Unit");
        resourceLayerMask = LayerMask.GetMask("Resource");
        buildingLayerMask = LayerMask.GetMask("Building");
    }

    void Start()
    {
        selectionBox = GameUI.instance.selectionBox;
        //Debug.Log(gameObject.name);
    }

    void Update()
    {
        // mouse down
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                //Debug.Log("Click UI 1");
         
                return;
            }

            ClearEverything();

            Debug.Log("Mouse Click");

            TrySelect(Input.mousePosition);

            _startPos = Input.mousePosition;

        }      

        // mouse held down
        if (Input.GetMouseButton(0))
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                //Debug.Log("Click UI 2");

                return;
            }

            Debug.Log("Mouse held down");

            //startPos = Input.mousePosition;

            UpdateSelectionBox(Input.mousePosition);
        }

        // mouse up
        if (Input.GetMouseButtonUp(0))
        {
            ReleaseSelectionBox();
        }

        if (_selected) //to update Info Panel all the time
        {
            if (_selectedUnits != null)
            {
                if (_selectedUnits.Count == 1)
                {
                    ShowUnit(_selectedUnits[0]);
                }
            }

            if (_curEnemy != null)
            {
                ShowEnemy();
            }
            else if (_curResource != null)
            {
                ShowResource();
            }
            else if (_curBuilding != null)
            {
                ShowBuilding();
            }
        }

    }

    //Clear Everything that was selected
    void ClearEverything()
    {
        ToggleSelectionVisual(false); //remove all currently selected units

        InfoManager.instance.ClearAllInfo(); //remove all info from panel
        ActionManager.instance.ClearAllInfo(); //remove all info from panel

        _curUnit = null; // remove current selected friendly unit
        _curEnemy = null; // remove current selected enemy unit
        _curResource = null; // remove current selected resource
        _curBuilding = null; // remove current selected building

        _selected = false; // clear to not select anything yet

        _selectedUnits = new List<Unit>(); //clear currently selected units and make a new blank list
    }

    //Called when we click on a unit
    void TrySelect(Vector2 screenPos)
    {
        Ray ray = _cam.ScreenPointToRay(screenPos);
        RaycastHit hit;

        // if the click hits Unit layer
        if (Physics.Raycast(ray, out hit, 1000, unitLayerMask))
        {
            Unit unit = hit.collider.GetComponent<Unit>();
            _curUnit = unit;

            if (_player.IsMyUnit(unit)) // Is it my unit?
            {
                _selected = true;

                _selectedUnits.Add(unit);
                unit.ToggleSelectionVisual(true);
                ShowUnit(_curUnit); //Show unit info in Info Panel
            }
            else
            {
                _selected = true;

                _curEnemy = unit;
                _curEnemy.ToggleSelectionVisual(true);//should show Enemy Red Toggle Selection Visual
                ShowEnemy(); //should show Enemy limited info in Info Panel
            }
        }

        // if the click hits Resource layer
        else if (Physics.Raycast(ray, out hit, 1000, resourceLayerMask))
        {
            ResourceSource rsrc = hit.collider.GetComponent<ResourceSource>();
            _curResource = rsrc;

            _selected = true;

            _curResource.ToggleSelectionVisual(true);
            ShowResource();//Show resource info
        }

        // if the click hits Building layer
        else if (Physics.Raycast(ray, out hit, 1000, buildingLayerMask))
        {
            Building building = hit.collider.GetComponent<Building>();
            _curBuilding = building;

            _selected = true;

            _curBuilding.ToggleSelectionVisual(true);

            ShowBuilding();//Show building info
        }
    }

    void ShowUnit(Unit u)
    {
        InfoManager.instance.ShowAllInfo(u); //Show unit info in Info Panel

        if (u.isWorker)
        {
            if (u.worker.curResourceSource != null || u.worker.gatherCurrent > 0)
            {
                InfoManager.instance.ShowRsrcInfo(u.worker.carryType, u.worker.gatherCurrent.ToString() + "/" + u.worker.gatherCapacity.ToString());
            }
            else
            {
                InfoManager.instance.HideResourceMode();
            }
        }

        ActionManager.instance.ShowCreateBuildingButtons(u);
    }

    void ShowEnemy()
    {
        InfoManager.instance.ShowAllInfo(_curEnemy);//should show Enemy limited info in Info Panel
        InfoManager.instance.HideResourceMode();
    }

    void ShowResource()
    {
        InfoManager.instance.ShowAllInfo(_curResource);//Show resource info in Info Panel
        InfoManager.instance.HideResourceMode();
    }

    void ShowBuilding()
    {
        InfoManager.instance.ShowAllInfo(_curBuilding);//Show building info in Info Panel
        ActionManager.instance.ShowCreateUnitButtons(_curBuilding);
        InfoManager.instance.HideResourceMode();
    }

    //Called when we are creating a selection box
    void UpdateSelectionBox (Vector2 curMousePos)
    {
        if (!selectionBox.gameObject.activeInHierarchy && _curBuilding == null && _curResource == null && _curEnemy == null && _curUnit == null)
        {
            selectionBox.gameObject.SetActive(true);
        }

        float width = curMousePos.x - _startPos.x;
        float height = curMousePos.y - _startPos.y;

        selectionBox.sizeDelta = new Vector2(Mathf.Abs(width), Mathf.Abs(height));
        selectionBox.anchoredPosition = _startPos + new Vector2(width / 2, height / 2);
    }

    void ReleaseSelectionBox ()
    {
        selectionBox.gameObject.SetActive(false);

        Vector2 min = selectionBox.anchoredPosition - (selectionBox.sizeDelta / 2);
        Vector2 max = selectionBox.anchoredPosition + (selectionBox.sizeDelta / 2);

        //Debug.Log("min = " + min);
        //Debug.Log("max = " + max);

        foreach (Unit unit in _player.units)
        {
            Vector3 screenPos = _cam.WorldToScreenPoint(unit.transform.position);

            if (screenPos.x > min.x && screenPos.x < max.x && screenPos.y > min.y && screenPos.y < max.y)
            {
                _selected = true;

                _selectedUnits.Add(unit);
                unit.ToggleSelectionVisual(true);
            }
        }

        if (_selectedUnits.Count > 0)
        {
            // cache the selected units in an array
            Unit[] Units = GetSelectedUnits();

            // send array of units to InfoManager
            InfoManager.instance.ShowGroupPic(Units);
        }

        selectionBox.sizeDelta = new Vector2(0, 0); //clear Selection Box's size;
    }

    // remove all destroyed or missing units from the selected list
    public void RemoveNullUnitsFromSelection()
    {
        for(int x = 0; x < _selectedUnits.Count; x++)
        {
            //Debug.Log(selectedUnits[x]);

            if(_selectedUnits[x] == null)
            {
                _selectedUnits.RemoveAt(x);
            }
        }

        //Debug.Log(selectedUnits.Count);
    }

    // toggle the selected units selection visual
    void ToggleSelectionVisual (bool selected)
    {
        foreach (Unit unit in _selectedUnits)
        {
            unit.ToggleSelectionVisual(selected);//remove current selected units selection visual
        }

        if (_curEnemy != null)
        _curEnemy.ToggleSelectionVisual(selected);//remove current selected enemy selection visual

        if (_curResource != null)
            _curResource.ToggleSelectionVisual(selected);//remove current selected resource selection visual

        if (_curBuilding != null)
            _curBuilding.ToggleSelectionVisual(selected);//remove current selected building selection visual
    }

    // returns whether or not we're selecting a unit or units
    public bool HasUnitsSelected ()
    {
        return _selectedUnits.Count > 0 ? true : false;
    }

    // returns the selected units
    public Unit[] GetSelectedUnits ()
    {
        return _selectedUnits.ToArray();
    }

}
