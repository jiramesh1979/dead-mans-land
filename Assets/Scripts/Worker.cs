﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worker : MonoBehaviour
{
    public ResourceSource curResourceSource;

    [SerializeField]
    private int _gatherAmount = 1; // An amount unit can gather every "gatherRate" second(s)

    [SerializeField]
    private float _gatherRate = 0.5f;

    public int gatherCurrent; // An amount currently carried by a unit
    public ResourceType carryType; // Resource type that is carrying
    public int gatherCapacity = 35; // Max amount a unit can carry

    private float _lastGatherTime;

    private Unit _unit;

    // Start is called before the first frame update
    void Start()
    {
        _unit = GetComponent<Unit>();
    }

    // Update is called once per frame
    void Update()
    {
        switch (_unit.state)
        {
            case UnitState.MoveToResource:
                {
                    MoveToResourceUpdate();
                    break;
                }
            case UnitState.Gather:
                {
                    GatherUpdate();
                    break;
                }
            case UnitState.MoveToHQ:
                {
                    MoveToHQUpdate();
                    break;
                }
            case UnitState.StoreAtHQ:
                {
                    StoreAtHQUpdate();
                    break;
                }
        }
    }

    // called every frame the 'MoveToResource' state is active
    void MoveToResourceUpdate()
    {
        if (curResourceSource == null)
        {
            _unit.SetState(UnitState.Idle);
            return;
        }

        if (Vector3.Distance(transform.position, _unit.GetNavAgent().destination) <= 1f)
        {
            _unit.SetState(UnitState.Gather);
        }
    }

    // called every frame the 'Gather' state is active
    void GatherUpdate()
    {
        if (curResourceSource == null)
        {
            FindNewResource();

            if (curResourceSource != null)
            {
                GatherResource(curResourceSource, curResourceSource.transform.position);
            }
            else
            {
                _unit.SetState(UnitState.RetreatToHQ);
            }

            return;
        }

        _unit.LookAt(curResourceSource.transform.position);

        if (Time.time - _lastGatherTime > _gatherRate)
        {
            _lastGatherTime = Time.time;

            if (gatherCurrent < gatherCapacity)
            {
                curResourceSource.GatherResource(_gatherAmount);

                carryType = curResourceSource.type;
                gatherCurrent += _gatherAmount;


                //Debug.Log(gatherCurrent.ToString());

                //Debug.Log(curResourceSource);
            }
            else
            {
                _unit.SetState(UnitState.MoveToHQ);
            }
        }
    }

    void MoveToHQUpdate()
    {
        if (curResourceSource == null)
        {
            _unit.SetState(UnitState.Idle);
            return;
        }

        if (Time.time - _unit.lastPathUpdateTime > _unit.pathUpdateRate)
        {
            _unit.lastPathUpdateTime = Time.time;
            _unit.GetNavAgent().isStopped = false;
            _unit.GetNavAgent().SetDestination(_unit.player.startPosition.position);
        }

        //Debug.Log(Vector3.Distance(transform.position, player.startPosition.position));

        if (Vector3.Distance(transform.position, _unit.player.startPosition.position) <= 4f)
            _unit.SetState(UnitState.StoreAtHQ);
    }

    void StoreAtHQUpdate()
    {
        if (curResourceSource == null && gatherCurrent <= 0)
        {
            _unit.SetState(UnitState.Idle);
            return;
        }

        _unit.LookAt(_unit.player.startPosition.position);

        if (gatherCurrent > 0)
        {
            _unit.player.GainResource(carryType, gatherCurrent);
            gatherCurrent = 0;
        }
        else if (curResourceSource != null)
        {
            _unit.GetNavAgent().destination = curResourceSource.transform.position;
            _unit.SetState(UnitState.MoveToResource);
        }
    }

    // move to a resource and begin to gather it
    public void GatherResource(ResourceSource resource, Vector3 pos)
    {
        curResourceSource = resource;

        if (curResourceSource.type != carryType)
        {
            carryType = curResourceSource.type;
            gatherCurrent = 0;
        }

        _unit.SetState(UnitState.MoveToResource);

        _unit.GetNavAgent().isStopped = false;
        _unit.GetNavAgent().SetDestination(pos);
    }

    void FindNewResource()
    {
        curResourceSource = _unit.player.GetClosestResource(transform.position, curResourceSource.type);
    }
}
