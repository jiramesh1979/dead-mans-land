﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharAnimation : MonoBehaviour
{
    private Animator _anim;
    private Unit _unit;

    // Start is called before the first frame update
    void Start()
    {
        _anim = GetComponentInChildren<Animator>();
        _unit = GetComponent<Unit>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_unit.state == UnitState.Idle)
        {
            _anim.SetBool("isIdle", true);
            _anim.SetBool("isMoving", false);
            _anim.SetBool("isGathering", false);
            _anim.SetBool("isAttacking", false);
        }

        if (_unit.state == UnitState.Move || _unit.state == UnitState.MoveToEnemy || 
            _unit.state == UnitState.MoveToEnemyBuilding || _unit.state == UnitState.MoveToHQ || 
            _unit.state == UnitState.MoveToResource || _unit.state == UnitState.MoveToBuild || _unit.state == UnitState.RetreatToHQ)
        {
            _anim.SetBool("isIdle", false);
            _anim.SetBool("isMoving", true);
            _anim.SetBool("isGathering", false);
            _anim.SetBool("isAttacking", false);
        }

        if (_unit.state == UnitState.Gather || _unit.state == UnitState.BuildProgress)
        {
            _anim.SetBool("isIdle", false);
            _anim.SetBool("isMoving", false);
            _anim.SetBool("isGathering", true);
            _anim.SetBool("isAttacking", false);
        }

        if (_unit.state == UnitState.AttackUnit || _unit.state == UnitState.AttackBuilding)
        {
            _anim.SetBool("isIdle", false);
            _anim.SetBool("isMoving", false);
            _anim.SetBool("isGathering", false);
            _anim.SetBool("isAttacking", true);
        }
    }
}
