﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Faction
{
    NAT = 0,
    USA = 1
}

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private Faction _myside;

    public Player[] players;

    public static GameManager instance;
    
    private Camera _cam;

    public Camera Cam { get { return _cam; } }

    void Awake()
    {
        instance = this;
        _cam = Camera.main;

        GetYourSide(Settings.instance.YourSide);
        CheckPlayers();
    }

    void GetYourSide(Faction side)
    {
        _myside = side;
    }

    void CheckPlayers()
    {
        foreach (Player p in players)
        {
            if (p.faction == _myside)
            {
                p.isMe = true;
                p.gameObject.AddComponent<UnitSelection>();
                p.gameObject.AddComponent<UnitCommander>();
            }
            else
            {
                p.isMe = false;
                p.gameObject.AddComponent<PlayerAI>();
            }
        }
    }

    // returns a random enemy player
    public Player GetRandomEnemyPlayer(Player me)
    {
        Player ranPlayer = players[UnityEngine.Random.Range(0, players.Length)];

        while(ranPlayer == me) //Random again if the Player is myself
        {
            ranPlayer = players[UnityEngine.Random.Range(0, players.Length)];
        }

        return ranPlayer;
    }

    // called when unit dies, check to see if there's one remaining player
    public void UnitDeathCheck()
    {
        int remainingPlayers = 0;
        Player winner = null;

        for (int x = 0; x < players.Length; x++)
        {
            if (players[x].units.Count > 0)
            {
                remainingPlayers++;
                winner = players[x];
            }
        }

        // if there is more than 1 remaining player, return
        if (remainingPlayers != 1)
            return;

        EndScreenUI.instance.SetEndScreen(winner.isMe);
    }

}
