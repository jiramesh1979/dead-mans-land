﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameUI : MonoBehaviour
{
    public TextMeshProUGUI unitCountText;
    public TextMeshProUGUI foodText;
    public TextMeshProUGUI woodText;
    public RectTransform selectionBox;
    public GameObject selectionMarker;

    public static GameUI instance;

    void Awake()
    {
        instance = this;
    }

    void Update()
    {   
        if (Input.GetKeyDown(KeyCode.U))
        {
            Canvas canvas = gameObject.GetComponent<Canvas>();

            if (canvas.enabled == true)
            {
                canvas.enabled = false;
            }
            else
            {
                canvas.enabled = true;
            }
        }
    }

    public void UpdateUnitCountText(int value, int limit)
    {
        unitCountText.text = value.ToString() + "/" + limit.ToString();
    }

    public void UpdateFoodText(int value)
    {
        foodText.text = value.ToString();
    }

    public void UpdateWoodText(int value)
    {
        woodText.text = value.ToString();
    }
}
