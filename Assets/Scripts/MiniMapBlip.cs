﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMapBlip : MonoBehaviour
{
    private GameObject _blip;

    public GameObject Blip { get { return _blip; } }

    // Start is called before the first frame update
    void Start()
    {
        _blip = GameObject.Instantiate(MiniMap.current.blipPrefab);
        _blip.transform.SetParent(MiniMap.current.transform);

        Unit unit = GetComponent<Unit>();

        _blip.GetComponent<Image>().color = unit.player.accentColor;
    }

    // Update is called once per frame
    void Update()
    {
        _blip.transform.position = MiniMap.current.worldToMinimapPos(transform.position);
    }

    void OnDestroy()
    {
        GameObject.Destroy(_blip);
    }
}
