﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float moveSpeed;
    public float zoomSpeed;

    public float minZoomDist;
    public float maxZoomDist;

    public float rotationAmount;
    public Quaternion newRotation;

    private Camera _cam;

    public Transform Corner1, Corner2;

    public static CameraController instance;

    private bool _zoomLock = false; //Zoom lock flag for Persperctive;
    private int _zoomLockDistance = 320; //Distance for Zoom lock

    void Awake()
    {
        instance = this;
        _cam = Camera.main;
    }

    void Start()
    {
        newRotation = transform.rotation;
    }

    void Update()
    {
        Move();
        Zoom();
        Rotate();
        ChangePerspective();
    }

    void ChangePerspective()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (_cam.orthographic)
            {
                _cam.orthographic = false;//3D perspective Camera
                _cam.fieldOfView = 30;

                if (!_zoomLock) //If Zoom lock is not done
                {
                    _cam.transform.position += _cam.transform.forward * _zoomLockDistance;
                    _zoomLock = true; //Zoom lock is done
                }
            }
            else
            {
                _cam.orthographic = true;
            }
        }

        //Debug.Log(cam.transform.position);
    }

    void Move()
    {
        float xInput = Input.GetAxis("Horizontal");
        float zInput = Input.GetAxis("Vertical");

        Vector3 dir = transform.forward * zInput + transform.right * xInput;

        if (transform.position.x < Corner1.position.x)
            return;

        if (transform.position.z < Corner1.position.z)
            return;

        if (transform.position.x > Corner2.position.x)
            return;

        if (transform.position.z > Corner2.position.z)
            return;

        transform.position += dir * moveSpeed * Time.deltaTime;

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, Corner1.position.x, Corner2.position.x),
                                    transform.position.y,
                                    Mathf.Clamp(transform.position.z, Corner1.position.z, Corner2.position.z));
    }

    void Zoom()
    {
        float scrollInput = Input.GetAxis("Mouse ScrollWheel");
        float dist = Vector3.Distance(transform.position, _cam.transform.position);
        //Debug.Log("Map = " + transform.position.ToString());
        //Debug.Log("Cam = " + cam.transform.position.ToString());
        //Debug.Log("Dist = " + dist.ToString());

        //Debug.Log("Scroll = " + scrollInput.ToString());

        if (dist < minZoomDist && scrollInput > 0.0f)
        {
            //Debug.Log("Too Close");
            return;
        }
        else if (dist > maxZoomDist && scrollInput < 0.0f)
        {
            //Debug.Log("Too Far");
            return;
        }

        _cam.transform.position += _cam.transform.forward * scrollInput * zoomSpeed;
    }

    void Rotate()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            newRotation *= Quaternion.Euler(Vector3.up * -rotationAmount);
        }

        if (Input.GetKey(KeyCode.E))
        {
            newRotation *= Quaternion.Euler(Vector3.up * rotationAmount);
        }

        transform.rotation = Quaternion.Lerp(transform.rotation, newRotation, Time.deltaTime * moveSpeed);
    }

    public void FocusOnPosition(Vector3 pos)
    {
        transform.position = pos;
    }



}
