﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Settings : MonoBehaviour
{
    public string newScene;

    [SerializeField]
    private Faction _yourSide;

    public Faction YourSide { get { return _yourSide; } }

    public static Settings instance;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlaySoundChoose()
    {
        
    }

    public void GoToMapNewGame()
    {
        SceneManager.LoadScene(newScene);
    }

    public void SelectSide(int side)
    {
        switch (side)
        {
            case 0:
                _yourSide = Faction.NAT;
                break;
            case 1:
                _yourSide = Faction.USA;
                break;
            default:
                break;
        }
    }
}
