﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitAI : MonoBehaviour
{
    public float checkRate = 1.0f;
    public float nearbyEnemyAttackRange = 30f;

    public LayerMask unitLayerMask;

    private PlayerAI _playerAI;
    private Unit _unit;

    public void InitializeAI(PlayerAI playerAI, Unit unit)
    {
        this._playerAI = playerAI;
        this._unit = unit;
    }

    void Start()
    {
        InvokeRepeating("Check", 0.0f, checkRate);
        unitLayerMask = LayerMask.GetMask("Unit");
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, nearbyEnemyAttackRange);
    }

    void Check()
    {
        // check if we have nearby enemies - if so, attack them
        if (_unit.state != UnitState.AttackUnit && _unit.state != UnitState.MoveToEnemy)
        {
            Unit potentialEnemy = CheckForNearbyEnemies();

            if (potentialEnemy != null)
                _unit.AttackUnit(potentialEnemy);
        }

        if (_unit.isWorker)
        {
            // if we're doing nothing, find a new resource
            if (_unit.state == UnitState.Idle)
                FindNewResource();
            // if we're moving to a resource which is destroyed, find a new one
            else if (_unit.state == UnitState.MoveToResource && _unit.worker.curResourceSource == null)
                FindNewResource();
        }
    }

    void FindNewResource()
    {
        ResourceSource resourceToGet = _playerAI.GetClosestResource(transform.position);

        if (resourceToGet != null)
            _unit.worker.GatherResource(resourceToGet, UnitMover.GetUnitDestinationsAroundSource(resourceToGet.transform.position));
        else
            PursueEnemy();
    }

    // checks for nearby enemies with a sphere cast
    Unit CheckForNearbyEnemies()
    {
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, nearbyEnemyAttackRange, Vector3.up, unitLayerMask);

        GameObject closest = null;
        float closestDist = 0.0f;

        for (int x = 0; x < hits.Length; x++)
        {
            //Debug.Log("Test - " + hits[x].collider.gameObject.ToString());

            // skip if this is not a unit
            if (hits[x].collider.gameObject.GetComponent<Unit>() == null)
                continue;

            // skip if this is us
            if (hits[x].collider.gameObject == gameObject)
                continue;

            // is this a team mate?
            if (_unit.player.IsMyUnit(hits[x].collider.GetComponent<Unit>()))
            {
                continue;
            }
            // if there is not closest enemy or the distance is less than the closest distance it currently has
            else if (!closest || Vector3.Distance(transform.position, hits[x].transform.position) < closestDist)
            {
                // if there is not closest enemy or the distance is less than the closest distance it currently has
                if (!closest || Vector3.Distance(transform.position, hits[x].transform.position) < closestDist)
                {
                    closest = hits[x].collider.gameObject;
                    closestDist = Vector3.Distance(transform.position, hits[x].transform.position);
                }
            }
        }

        if (closest != null)
        {
            Debug.Log(closest.gameObject.ToString() + ", " + closestDist.ToString());
            return closest.GetComponent<Unit>();
        }
        else
            return null;
    }

    // called when there's no more resources - chase after a random enemy
    void PursueEnemy()
    {
        Player enemyPlayer = GameManager.instance.GetRandomEnemyPlayer(_unit.player);

        if (enemyPlayer.units.Count > 0)
            _unit.AttackUnit(enemyPlayer.units[Random.Range(0, enemyPlayer.units.Count)]);
    }
}
