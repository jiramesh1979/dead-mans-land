﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Builder : MonoBehaviour
{
    public bool toBuild = false;
    public bool showGhost = false;

    public GameObject[] buildingList; // Buildings that this unit can build
    public GameObject[] ghostBuildingList; // Transparent buildings according to building list

    [SerializeField]
    private GameObject _newBuilding; // Current building to build
    [SerializeField]
    private GameObject _ghostBuilding; // Tranparent building to check site to build

    public GameObject NewBuilding { get { return _newBuilding; } set { _newBuilding = value; } }
    public GameObject GhostBuilding { get { return _ghostBuilding; } set { _ghostBuilding = value; } }

    private Unit _unit;

    int rotation = 90;
    int finalRotation = 0;

    // Start is called before the first frame update
    void Start()
    {
        _unit = GetComponent<Unit>();
    }

    // Update is called once per frame
    void Update()
    {
        if (toBuild == true) // if this unit is to build something
        {
            BuildingToPlaceFollowsMouse();

            if (Input.GetMouseButtonDown(1)) //Rotate ghost building
            {
                finalRotation += rotation;

                if (finalRotation >= 360)
                    finalRotation = 0;

                //Debug.Log(finalRotation);

                _ghostBuilding.transform.Rotate (0, rotation, 0, Space.Self);
            }

            if (Input.GetMouseButtonDown(0)) //Click to select a building site
            {
                Ray ray = GameManager.instance.Cam.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit))
                {
                    Debug.Log(hit.collider.tag);

                    //if (hit.collider.CompareTag("Ground"))
                    //if (hit.collider.CompareTag("GhostBuilding"))
                    //if (ghostBuilding.GetComponent<FindBuildingSite>().canBuild == true)
                    //if ((hit.collider.CompareTag("GhostBuilding")) && (ghostBuilding.GetComponent<FindBuildingSite>().canBuild == true))
                    if ((hit.collider.CompareTag("Ground")) && (_ghostBuilding.GetComponent<FindBuildingSite>().CanBuild == true))
                    {
                        //Debug.Log("Can build here");
                        PrepareToBuild(); //Unit prepares to move to building site.

                        CreateBuildingSite(hit.point); //Create building site with 1 HP

                        //Pseudo code
                        //If its collider hit something, it cannot be built here.
                    }
                    else
                    {
                        Debug.Log("Cannot build here - " + _ghostBuilding.GetComponent<FindBuildingSite>().CanBuild);
                    }
                }
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                CancelToBuild();
            }
        }

        switch(_unit.state)
        {
            case UnitState.MoveToBuild:
                {
                    MoveToBuild(_newBuilding);
                    break;
                }
            case UnitState.BuildProgress:
                {
                    BuildProgress();
                    break;
                }
        }
    }

    void PrepareToBuild()
    {
        //Debug.Log(newBuilding.transform.position);

        _unit.SetState(UnitState.MoveToBuild);           
    }

    void MoveToBuild(GameObject b)
    {
        //Debug.Log("Distance is " + Vector3.Distance(transform.position, b.transform.position));

        _unit.GetNavAgent().isStopped = false;
        _unit.GetNavAgent().SetDestination(b.transform.position);

        if (Vector3.Distance(transform.position, b.transform.position) <= b.GetComponent<Building>().radiusFromCenter)
        {
            _unit.GetNavAgent().isStopped = true;
            _unit.SetState(UnitState.BuildProgress);
        }
    }

    void CreateBuildingSite(Vector3 pos)
    {
        if (_ghostBuilding != null)
        {
            Destroy(_ghostBuilding);
            _ghostBuilding = null;
        }

        //We use prefab position.y when instantiating.
        GameObject buildingObj = Instantiate(_newBuilding, new Vector3(pos.x, _newBuilding.transform.position.y, pos.z), Quaternion.Euler(0, finalRotation, 0), transform);

        Building building = buildingObj.GetComponent<Building>();

        //Set building to be underground
        buildingObj.transform.position = new Vector3(buildingObj.transform.position.x, 
                                                     buildingObj.transform.position.y - building.intoTheGround,
                                                     buildingObj.transform.position.z);
        
        //Set building's parent game object
        buildingObj.transform.parent = _unit.player.buildingsParent.transform;

        _newBuilding = buildingObj; //set a new clone building object to be a building in Unit's mind instead of a prototype      

        _unit.player.buildings.Add(building);

        building.player = _unit.player; //set a building's faction to be belong to this player
        building.isFunctional = false;
        building.CurHP = 1;

        _unit.player.wood -= building.cost;

        toBuild = false; //Disable ghost building

        //buildingToPlace = null;
        //unit.SetState(UnitState.Idle);

        if (_unit.player.isMe)
        {
            GameUI.instance.UpdateWoodText(_unit.player.wood);
        }

        Debug.Log("Building site created.");
    }

    void BuildProgress()
    {
        Building b = _newBuilding.GetComponent<Building>();

        b.Timer += Time.deltaTime;

        if (b.Timer >= b.waitTime)
        {
            b.Timer = 0;

            b.CurHP++;

            //Raise up building from the ground
            _newBuilding.transform.position += new Vector3(0, b.intoTheGround / (b.maxHP - 1), 0);

            if (b.CurHP >= b.maxHP)
            {
                b.CurHP = b.maxHP;
                b.isFunctional = true;

                _newBuilding.transform.position = new Vector3(_newBuilding.transform.position.x, _newBuilding.transform.position.y, _newBuilding.transform.position.z);
                _unit.SetState(UnitState.Idle);
            }
        }
    }

    void BuildingToPlaceFollowsMouse()
    {
        if (showGhost == true)
        {
            Ray ray = GameManager.instance.Cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                _ghostBuilding.transform.position = new Vector3(hit.point.x, 0, hit.point.z);
            }
        }
    }

    public void ToCreateNewBuilding(int i)
    {
        if (_unit.player.wood - buildingList[i].gameObject.GetComponent<Building>().cost < 0)
        {
            return;
        }
        else
        {
            toBuild = true;
            //Debug.Log("To build");
        }

        _newBuilding = buildingList[i]; //Set prefab into new building

        Ray ray = GameManager.instance.Cam.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            //Debug.Log(rotation);

            _ghostBuilding = Instantiate(ghostBuildingList[i], hit.point, Quaternion.identity, transform);
        }

        showGhost = true;
    }

    void CancelToBuild()
    {
        toBuild = false;
        showGhost = false;

        _newBuilding = null;
        Destroy(_ghostBuilding);
        _ghostBuilding = null;

        //Debug.Log("Cancel Building");
    }

    void OnDestroy()
    {
        if (_ghostBuilding != null)
            Destroy(_ghostBuilding);

        //if (buildingToPlace != null)
        //Destroy(buildingToPlace);
    }
}
