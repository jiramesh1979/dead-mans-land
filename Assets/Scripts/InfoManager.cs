﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
using System;

public class InfoManager : MonoBehaviour
{
    public static InfoManager instance;

    public Image unitPic;

    public GameObject info;
    public GameObject picGroup;

    public Image[] groupPic;
    public Image[] recruitPic;

    public ProgressBar progressBar;

    public Image HPIcon;

    public GameObject rsrcSection;
    public GameObject progressSection;
    public Image rsrcIcon;
    public Sprite foodSprite;
    public Sprite woodSprite;

    public TextMeshProUGUI Name, HP, Def, Attk, Rsrc;

    public InfoManager()
    {
        instance = this;
    }

    public void SetPic(Sprite pic)
    {
        //Unit Pic
        unitPic.sprite = pic;
        unitPic.color = Color.white;

        //HPIcon
        HPIcon.color = Color.white;
    }

    public void ClearPic()
    {
        //Unit Pic
        unitPic.color = Color.clear;

        //HPIcon
        HPIcon.color = Color.clear;

        //Resource Icon
        rsrcIcon.color = Color.clear;
    }

    public void SetName(string name)
    {
        Name.text = name;
    }

    public void SetHP(string HPText)
    {
        HP.text = HPText;
    }

    public void SetRsrc(string rsrcText)
    {
        Rsrc.text = rsrcText;
    }

    public void ShowRsrcInfo(ResourceType rType, string rsrc)
    {
        ShowResourceMode();
        SelectRsrcIcon(rType);
        SetRsrc(rsrc);
    }

    public void ShowProgressMode(List<GameObject> units, float progress)
    {
        progressSection.SetActive(true);
        ShowRecruitList(units);
        ShowProgressBar(progress);
    }

    void ShowRecruitList(List<GameObject> units)
    {
        for (int i = 0; i < units.Count; i++)
        {
            recruitPic[i].gameObject.SetActive(true);
          
            recruitPic[i].sprite = units[i].GetComponent<Unit>().unitPic;
            recruitPic[i].color = Color.white;
        }
    }

    void ShowProgressBar(float progress)
    {
        progressBar.gameObject.SetActive(true);
        progressBar.UpdateProgressBar(progress);
    }

    public void HideProgressMode()
    {
        progressSection.SetActive(false);
        HideRecruitList();
        HideProgressBar();
    }

    void HideRecruitList()
    {
        for (int i = 0; i < recruitPic.Length; i++)
        {
            recruitPic[i].gameObject.SetActive(false);
            recruitPic[i].sprite = null;
            recruitPic[i].color = Color.clear;
        }
    }

    void HideProgressBar()
    {
        progressBar.gameObject.SetActive(false);
        progressBar.slider.value = 0;
    }

    public void ClearRecruitList() //To clear an old pictures
    {
        for (int i = 0; i < recruitPic.Length; i++)
        {
            recruitPic[i].sprite = null;
            recruitPic[i].color = Color.clear;
        }
    }

    public void SelectRsrcIcon(ResourceType rt)
    {
        //Resource Icon
        rsrcIcon.color = Color.white;

        switch (rt)
        {
            case ResourceType.Food:
                {
                    rsrcIcon.sprite = foodSprite;
                    break;
                }

            case ResourceType.Wood:
                {
                    rsrcIcon.sprite = woodSprite;
                    break;
                }
        }
    }

    public void ClearLines()
    {
        SetName("");
        SetHP("");
        SetRsrc("");
    }

    public void ShowAllInfo(Unit unit)
    {
        ChangeToSingleMode();

        SetPic(unit.unitPic);
        SetName(unit.unitName);
        SetHP(unit.CurHP.ToString() + "/" + unit.maxHP.ToString());
    }

    public void ShowAllInfo(ResourceSource r)
    {
        ChangeToSingleMode();

        SetPic(r.rsrcPic);
        SetName(r.rsrcName);
        SetHP(r.Quantity.ToString() + "/" + r.maxQuantity.ToString());
    }

    public void ShowAllInfo(Building b)
    {
        ChangeToSingleMode();

        if (b.recruitList.Count > 0)
        {
            ShowProgressMode(b.recruitList, b.Progress);
        }

        SetPic(b.buildingPic);
        SetName(b.buildingName);
        SetHP(b.CurHP.ToString() + "/" + b.maxHP.ToString());
    }

    public void ClearAllInfo()
    {
        ClearPic();
        ClearLines();
        ClearPicGroup();
        ClearRecruitList();
        HideProgressBar();
    }

    public void ChangeToGroupMode()
    {
        ClearAllInfo();
        unitPic.gameObject.SetActive(false);
        info.SetActive(false);

        picGroup.SetActive(true);
        ClearPicGroup();
    }

    public void ClearPicGroup()
    {
        for (int x = 0; x < groupPic.Length; x++)
        {
            groupPic[x].sprite = null;
            groupPic[x].color = Color.clear;
        }
    }

    public void ShowResourceMode()
    {
        rsrcSection.SetActive(true);
    }

    public void HideResourceMode()
    {
        rsrcSection.SetActive(false);
    }

    public void ChangeToSingleMode()
    {
        unitPic.gameObject.SetActive(true);
        info.SetActive(true);

        picGroup.SetActive(false);
    }

    public void ShowGroupPic(Unit[] units)
    {
        ChangeToGroupMode();

        for (int x = 0; x < units.Length; x++)
        {
            groupPic[x].gameObject.SetActive(true);
            groupPic[x].sprite = units[x].unitPic;
            groupPic[x].color = Color.white;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        ClearAllInfo();
    }

}
