﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum ResourceType
{
    Food,
    Wood,
    Stone
}

public class ResourceSource : MonoBehaviour
{
    public string rsrcName;
    public Sprite rsrcPic;

    public ResourceType type;

    [SerializeField]
    private int _quantity;
    public int maxQuantity;

    public int Quantity { get { return _quantity; } }

    [Header("Components")]
    public GameObject SelectionVisual; //Selection Ring

    //events
    public UnityEvent onRsrcQuantityChange;
    public UnityEvent onInfoQuantityChange;

    void Start()
    {
        _quantity = maxQuantity;
    }

    //called when a unit gathers the resource
    public void GatherResource(int amount)
    {
        _quantity -= amount;

        int amountToGive = amount;

        // make sure we don't give more than we have
        if (_quantity < 0)
        {
            amountToGive = amount + _quantity;
        }

        // if we're depleted, delete the resource
        if(_quantity <= 0)
        {
            Destroy(gameObject);
        }

        // To call the 'onResourceQuantityChange' method in ResourcSourceUI
        if(onRsrcQuantityChange != null)
        {
            onRsrcQuantityChange.Invoke();
        }
    }

    // toggles green selection ring around resource
    public void ToggleSelectionVisual(bool selected)
    {
        if (SelectionVisual != null)
            SelectionVisual.SetActive(selected);
    }
}
