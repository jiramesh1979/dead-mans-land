﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class UnitCommander : MonoBehaviour
{
    public GameObject selectionMarkerPrefab;
    public LayerMask layerMask; // to keep Ground and Resource Layer

    // components
    private UnitSelection _unitSelection;
    private Camera _cam;

    private Player _player;

    void Awake()
    {
        // get the components
        _unitSelection = GetComponent<UnitSelection>();
        _cam = Camera.main;

        selectionMarkerPrefab = GameUI.instance.selectionMarker;
        layerMask = LayerMask.GetMask("Unit", "Ground", "Resource", "Building");
    }

    void Start()
    {
        _player = GetComponent<Player>();    
    }

    void Update()
    {
        // did we press down our right mouse button and do we have units selected?
        if(Input.GetMouseButtonDown(1) && _unitSelection.HasUnitsSelected())
        {
            if (EventSystem.current.IsPointerOverGameObject())
            {
                Debug.Log("Right Click UI 1");

                return;
            }

            Ray ray = _cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            _unitSelection.RemoveNullUnitsFromSelection();

            // cache the selected units in an array
            Unit[] selectedUnits = _unitSelection.GetSelectedUnits();

            if (CheckIfTheyAreToBuild(selectedUnits))
                return;

            // shoot the raycast
            if(Physics.Raycast(ray, out hit, 1000, layerMask))
            {
                // did we click on a building?
                if (hit.collider.CompareTag("Building"))
                {
                    Building building = hit.collider.gameObject.GetComponent<Building>();

                    //Debug.Log("Click Building");

                    if (building == null)
                    {
                        return;
                    }

                    if (!_player.IsMyBuilding(building)) // if it is an enemy's building
                    {
                        UnitAttackEnemyBuilding(building, selectedUnits);
                        CreateSelectionMarker(building.transform.position, false);
                    }
                    else // it is my building
                    {
                        if (building.isHQ)
                        {
                            UnitDeliverAtHQ(selectedUnits);
                        }

                        if (building.CurHP < building.maxHP)
                        {
                            UnitHelpFixBuilding(hit.collider.gameObject, selectedUnits);
                        }
                    }
                }

                // did we click on an enemy?
                else if (hit.collider.CompareTag("Unit"))
                {
                    Unit unit = hit.collider.gameObject.GetComponent<Unit>();

                    //Debug.Log(unit);

                    //Debug.Log(player.faction);

                    if (!_player.IsMyUnit(unit))// if it is an enemy's unit
                    {
                        //Debug.Log("Attack " + unit);

                        UnitAttackEnemy(unit, selectedUnits);
                        unit.greenSelectionVisual.SetActive(true);
                        //unit.greenSelectionVisual.SetActive(false);

                        //CreateSelectionMarker(unit.transform.position, false);
                    }
                }

                // are we clicking on a resource?
                else if (hit.collider.CompareTag("Resource"))
                {
                    UnitsGatherResource(hit.collider.GetComponent<ResourceSource>(), selectedUnits);
                    CreateSelectionMarker(hit.collider.transform.position, true);
                }

                // are we clicking on the ground?
                else if (hit.collider.CompareTag("Ground"))
                {
                    UnitsMoveToPosition(hit.point, selectedUnits);
                    CreateSelectionMarker(hit.point, false);

                    //Debug.Log("Click Ground");
                }

            }
        }
    }

    bool CheckIfTheyAreToBuild(Unit[] selectedUnits)
    {
        foreach (Unit u in selectedUnits)
        {
            if (u.isBuilder)
            {
                if (u.builder.toBuild)
                    return true;
            }
        }

        return false;
    }

    // called when we command units to move somewhere
    void UnitsMoveToPosition (Vector3 movePos, Unit[] units)
    {
        Vector3[] destinations = UnitMover.GetUnitGroupDestinations(movePos, units.Length, 2);

        for (int x = 0; x < units.Length; x++)
        {
            units[x].MoveToPosition(destinations[x]);
        }
    }

    // called when we command units to gather a resource
    void UnitsGatherResource(ResourceSource resource, Unit[] units)
    {
        if(units.Length == 1)
        {
            if (units[0].isBuilder)
            {
                units[0].worker.GatherResource(resource, UnitMover.GetUnitDestinationsAroundSource(resource.transform.position));
            }
            else
            {
                units[0].MoveToPosition(UnitMover.GetUnitDestinationsAroundSource(resource.transform.position));
            }
        }
        else
        {
            Vector3[] destinations = UnitMover.GetUnitGroupDestinationsAroundSource(resource.transform.position, units.Length);

            for(int x = 0; x < units.Length; x++)
            {
                if (units[x].isBuilder)
                {
                    units[x].worker.GatherResource(resource, destinations[x]);
                }
                else
                {
                    units[x].MoveToPosition(destinations[x]);
                }
            }
        }
    }

    // called when we command units to manually deliver carrying resources to HQ
    void UnitDeliverAtHQ(Unit[] units)
    {
        if (units.Length == 1)
        {
            units[0].worker.curResourceSource = null;
            units[0].SetState(UnitState.RetreatToHQ);           
        }
        //else
        //{
        //    Vector3[] destinations = UnitMover.GetUnitGroupDestinationsAroundSource(resource.transform.position, units.Length);

        //    for (int x = 0; x < units.Length; x++)
        //    {
        //        if (units[x].isWorker)
        //        {
        //            units[x].GatherResource(resource, destinations[x]);
        //        }
        //        else
        //        {
        //            units[x].MoveToPosition(destinations[x]);
        //        }
        //    }
        //}
    }

    void UnitHelpFixBuilding(GameObject target, Unit[] units)
    {
        if ((units.Length == 1) && (units[0].isBuilder))
        {
            units[0].builder.NewBuilding = target;
            units[0].SetState(UnitState.MoveToBuild);
        }
        else
        {
            for (int i = 0; i < units.Length; i++)
            {
                if (units[i].isBuilder)
                {
                    units[i].builder.NewBuilding = target;
                    units[i].SetState(UnitState.MoveToBuild);
                }
            }
        }

    }

    void UnitAttackEnemy(Unit target, Unit[] units)
    {
        for(int x = 0; x < units.Length; x++)
        {
            units[x].AttackUnit(target);
        }
    }

    void UnitAttackEnemyBuilding(Building target, Unit[] units)
    {
        for (int x = 0; x < units.Length; x++)
        {
            units[x].AttackBuilding(target);
        }
    }

    // create a new seletion marker visual at given position
    void CreateSelectionMarker(Vector3 pos, bool large)
    {
        GameObject marker = Instantiate(selectionMarkerPrefab, new Vector3(pos.x, 0.1f, pos.z),  Quaternion.identity);

        if(large)
            marker.transform.localScale = new Vector3 (0.2f, 0.2f, 0.2f);
    }

}
