﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public enum UnitState
{
    Idle,
    Move,
    MoveToResource,
    Gather,
    MoveToHQ,
    StoreAtHQ,
    MoveToEnemy,
    AttackUnit,
    MoveToEnemyBuilding,
    AttackBuilding,
    RetreatToHQ,
    MoveToBuild,
    BuildProgress
}

public class Unit : MonoBehaviour
{
    [Header("Stats")]
    public string unitName;
    public Sprite unitPic;

    public UnitState state;

    public bool isCommander;
    public bool isBuilder;
    public bool isWorker;

    [SerializeField]
    private int _curHP;
    public int maxHP;

    public int CurHP { get { return _curHP; } }

    public int minAttackDamage;
    public int maxAttackDamage;

    public float attackRate;
    public float lastAttackTime;

    public float attackDistance;

    public float pathUpdateRate = 1.0f;
    public float lastPathUpdateTime; 

    public float progressRate; // How quick a unit's progress bar is when it is recruited

    private Unit curEnemyUnitTarget;
    private Building curEnemyBuildingTarget;

    [Header("Components")]
    public GameObject greenSelectionVisual; //Green Selection Ring

    private NavMeshAgent navAgent;
    public UnitHealthBar healthBar;

    public Player player;
    public Builder builder;
    public Worker worker;

    public float unitWaitTime; //How fast the unit is recruited. Lower is faster.

    // events
    [System.Serializable]
    public class StateChangeEvent : UnityEvent<UnitState> { }
    public StateChangeEvent onStateChange;

    void Awake()
    {
        // get the components
        navAgent = GetComponent<NavMeshAgent>();

        // get the components
        builder = GetComponent<Builder>();
        worker = GetComponent<Worker>();

        SetState(UnitState.Idle);
    }

    void Start()
    {
        _curHP = maxHP;
    }

    public float GetUnitWaitTime()
    {
        return unitWaitTime;
    }

    public void SetState(UnitState toState)
    {
        state = toState;

        // calling the event
        if(onStateChange != null)
        {
            onStateChange.Invoke(state);
        }

        if(state ==  UnitState.Idle)
        {
            navAgent.isStopped = true;
            navAgent.ResetPath();
        }
    }

    void Update()
    {
        switch(state)
        {
            case UnitState.Move:
                {
                    MoveUpdate();
                    break;
                }
            case UnitState.MoveToEnemy:
                {
                    MoveToEnemyUpdate();
                    break;
                }
            case UnitState.AttackUnit:
                {
                    AttackUpdate();
                    break;
                }
            case UnitState.MoveToEnemyBuilding:
                {
                    MoveToEnemyBuildingUpdate();
                    break;
                }
            case UnitState.AttackBuilding:
                {
                    AttackBuildingUpdate();
                    break;
                }
            case UnitState.RetreatToHQ:
                {
                    RetreatToHQ();
                    break;
                }
        }
    }

    public NavMeshAgent GetNavAgent()
    {
        return navAgent;
    }

    // called every frame the 'Move' state is active
    void MoveUpdate()
    {
        if (Vector3.Distance(transform.position, navAgent.destination) <= 0.1f)
            SetState(UnitState.Idle);
    }

    // called every frame the 'MoveToEnemy' state is active
    void MoveToEnemyUpdate()
    {
        if (curEnemyUnitTarget == null)
        {
            SetState(UnitState.Idle);
            return;
        }

        if (Time.time - lastPathUpdateTime > pathUpdateRate)
        {
            lastPathUpdateTime = Time.time;
            navAgent.isStopped = false;
            navAgent.SetDestination(curEnemyUnitTarget.transform.position);
        }

        if (Vector3.Distance(transform.position, curEnemyUnitTarget.transform.position) <= attackDistance)
        {
            SetState(UnitState.AttackUnit);
        }
    }

    // called every frame the 'Attack' state is active
    void AttackUpdate()
    {
        // if our target is dead, go idle
        if (curEnemyUnitTarget == null)
        {
            SetState(UnitState.Idle);
            return;
        }

        // if we're still moving, stop
        if (!navAgent.isStopped)
        {
            navAgent.isStopped = true;
        }

        // attack every 'attackRate' seconds
        if (Time.time - lastAttackTime > attackRate)
        {
            lastAttackTime = Time.time;

            AudioManager.instance.PlayRandomGunSFX();
            curEnemyUnitTarget.TakeDamage(UnityEngine.Random.Range(minAttackDamage, maxAttackDamage + 1));
        }

        // look at the enemy
        LookAt(curEnemyUnitTarget.transform.position);

        // if we're too far away, move towards the enemy
        if (Vector3.Distance(transform.position, curEnemyUnitTarget.transform.position) > attackDistance)
        {
            SetState(UnitState.MoveToEnemy);
        }
    }

    // called every frame the 'MoveToEnemyBuilding' state is active
    void MoveToEnemyBuildingUpdate()
    {
        if (curEnemyBuildingTarget == null)
        {
            SetState(UnitState.Idle);
            return;
        }

        if (Time.time - lastPathUpdateTime > pathUpdateRate)
        {
            lastPathUpdateTime = Time.time;
            navAgent.isStopped = false;
            navAgent.SetDestination(curEnemyBuildingTarget.transform.position);
        }

        if ((Vector3.Distance(transform.position, curEnemyBuildingTarget.transform.position) - 4f) <= attackDistance)
        {
            SetState(UnitState.AttackBuilding);
        }
    }

    // called every frame the 'AttackBuilding' state is active
    void AttackBuildingUpdate()
    {
        // if our target is dead, go idle
        if (curEnemyBuildingTarget == null)
        {
            SetState(UnitState.Idle);
            return;
        }

        // if we're still moving, stop
        if (!navAgent.isStopped)
        {
            navAgent.isStopped = true;
        }

        // attack every 'attackRate' seconds
        if (Time.time - lastAttackTime > attackRate)
        {
            lastAttackTime = Time.time;

            AudioManager.instance.PlayRandomGunSFX();
            curEnemyBuildingTarget.TakeDamage(UnityEngine.Random.Range(minAttackDamage, maxAttackDamage + 1));
        }

        // look at the enemy
        LookAt(curEnemyBuildingTarget.transform.position);

        // if we're too far away, move towards the enemy's building
        if ((Vector3.Distance(transform.position, curEnemyBuildingTarget.transform.position) - 4f) > attackDistance)
        {
            SetState(UnitState.MoveToEnemyBuilding);
        }
    }

    void RetreatToHQ()
    {
        if (Time.time - lastPathUpdateTime > pathUpdateRate)
        {
            lastPathUpdateTime = Time.time;
            navAgent.isStopped = false;
            navAgent.SetDestination(player.startPosition.position);
        }

        if (Vector3.Distance(transform.position, player.startPosition.position) <= 4f)
            SetState(UnitState.StoreAtHQ);
    }

    // called when an enemy unit attacks us
    public void TakeDamage(int damage)
    {
        _curHP -= damage;

        if (_curHP <= 0)
        {
            _curHP = 0;
            Die();
        }

        // update the health bar
        healthBar.UpdateHealthBar(_curHP, maxHP);
    }

    // called when our health reaches zero
    void Die()
    {
        player.units.Remove(this);

        InfoManager.instance.ClearAllInfo();

        GameManager.instance.UnitDeathCheck();

        Destroy(gameObject);
    }

    // move the units to a specific position
    public void MoveToPosition(Vector3 pos)
    {        
        navAgent.SetDestination(pos);

        navAgent.isStopped = false;

        SetState(UnitState.Move);
    }

    // move to an enemy unit and attack them
    public void AttackUnit(Unit target)
    {
        curEnemyUnitTarget = target;
        SetState(UnitState.MoveToEnemy);
    }

    // move to an enemy building and attack them
    public void AttackBuilding(Building target)
    {
        curEnemyBuildingTarget = target;
        SetState(UnitState.MoveToEnemyBuilding);
    }

    // toggles green selection ring around our feet
    public void ToggleSelectionVisual(bool selected)
    {
        if (greenSelectionVisual != null)
            greenSelectionVisual.SetActive(selected);
    }

    // rotate to face the given position
    public void LookAt(Vector3 pos)
    {
        Vector3 dir = (pos - transform.position).normalized;
        float angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(0, angle, 0);
    }
}
