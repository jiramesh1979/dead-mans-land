﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public AudioSource[] BGM;
    public AudioSource[] gunSFX;
    public AudioSource titleMusic;
    public AudioSource briefMusic;
    public AudioSource afterPlayMusic;
    public AudioSource creditsMusic;

    private int currentSong = 0;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayGunSFX(int soundToPlay)
    {
        if (soundToPlay < gunSFX.Length)
        {
            gunSFX[soundToPlay].Play();
        }
    }

    public void PlayRandomGunSFX()
    {
        int sound = RandomAudio(0, gunSFX.Length);

        if (sound < gunSFX.Length)
        {
            gunSFX[sound].Play();
        }
    }

    public void PlayBGM(int musicToPlay)
    {
        if (!BGM[musicToPlay].isPlaying)
        {
            StopMusic();

            if (musicToPlay < BGM.Length)
            {
                BGM[musicToPlay].Play();
            }
        }
    }

    public void StopMusic()
    {
        for (int i = 0; i < BGM.Length; i++)
        {
            BGM[i].Stop();
        }
    }

    public int RandomAudio(int min, int max)
    {
        return Random.Range(min, max);
    }

    public void PlayRandomMusic()
    {
        if (CheckPlaying() == false)
        {
            currentSong = RandomAudio(0, BGM.Length);
            PlayBGM(currentSong);
        }
    }

    public bool CheckPlaying()
    {
        if (BGM[currentSong].isPlaying)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public void PlayTitleMusic()
    {
        if (!titleMusic.isPlaying)
        {
            titleMusic.Play();
        }
    }

    public void StopTitleMusic()
    {
        titleMusic.Stop();
    }
}
