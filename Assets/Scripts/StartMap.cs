﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMap : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if (AudioManager.instance != null)
        {
            AudioManager.instance.StopMusic();
            AudioManager.instance.StopTitleMusic();
            AudioManager.instance.PlayRandomMusic();
        }
    }

    // Update is called once per frame
    void Update()
    {
        AudioManager.instance.PlayRandomMusic();
    }
}
