﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public enum UnitState
{
    Idle,
    Move,
    MoveToResource,
    Gather,
    MoveToHQ,
    StoreAtHQ,
    MoveToEnemy,
    AttackUnit,
    MoveToEnemyBuilding,
    AttackBuilding,
    RetreatToHQ,
    MoveToBuild,
    BuildProgress
}

public class Unit : MonoBehaviour
{
    [Header("Stats")]
    public string unitName;
    public Sprite unitPic;

    public UnitState state;

    public bool isBuilder;

    public int curHP;
    public int maxHP;

    public int minAttackDamage;
    public int maxAttackDamage;

    public float attackRate;
    private float lastAttackTime;

    public float attackDistance;

    public float pathUpdateRate = 1.0f;
    private float lastPathUpdateTime; 

    public int gatherAmount; // An amount unit can gather every "gatherRate" second(s)
    public float gatherRate;
    public int gatherCurrent; // An amount currently carried by a unit
    public ResourceType carryType; // Resource type that is carrying
    public int gatherCapacity; // Max amount a unit can carry

    private float lastGatherTime;

    public float progressRate; // How quick a unit's progress bar is when it is recruited

    public ResourceSource curResourceSource;
    private Unit curEnemyUnitTarget;
    private Building curEnemyBuildingTarget;

    [Header("Components")]
    public GameObject greenSelectionVisual; //Green Selection Ring

    private NavMeshAgent navAgent;
    public UnitHealthBar healthBar;

    public Player player;
    public Builder builder;

    public float unitWaitTime; //How fast the unit is recruited. Lower is faster.

    // events
    [System.Serializable]
    public class StateChangeEvent : UnityEvent<UnitState> { }
    public StateChangeEvent onStateChange;

    void Awake()
    {
        // get the components
        navAgent = GetComponent<NavMeshAgent>();

        // get the components
        builder = GetComponent<Builder>();

        SetState(UnitState.Idle);
    }

    void Start()
    {

    }

    public float GetUnitWaitTime()
    {
        return unitWaitTime;
    }

    public void SetState(UnitState toState)
    {
        state = toState;

        // calling the event
        if(onStateChange != null)
        {
            onStateChange.Invoke(state);
        }

        if(state ==  UnitState.Idle)
        {
            navAgent.isStopped = true;
            navAgent.ResetPath();
        }
    }

    void Update()
    {
        switch(state)
        {
            case UnitState.Move:
                {
                    MoveUpdate();
                    break;
                }
            case UnitState.MoveToResource:
                {
                    MoveToResourceUpdate();
                    break;
                }
            case UnitState.Gather:
                {
                    GatherUpdate();
                    break;
                }
            case UnitState.MoveToHQ:
                {
                    MoveToHQUpdate();
                    break;
                }
            case UnitState.StoreAtHQ:
                {
                    StoreAtHQUpdate();
                    break;
                }
            case UnitState.MoveToEnemy:
                {
                    MoveToEnemyUpdate();
                    break;
                }
            case UnitState.AttackUnit:
                {
                    AttackUpdate();
                    break;
                }
            case UnitState.MoveToEnemyBuilding:
                {
                    MoveToEnemyBuildingUpdate();
                    break;
                }
            case UnitState.AttackBuilding:
                {
                    AttackBuildingUpdate();
                    break;
                }
            case UnitState.RetreatToHQ:
                {
                    RetreatToHQ();
                    break;
                }
        }
    }

    public NavMeshAgent GetNavAgent()
    {
        return navAgent;
    }

    // called every frame the 'Move' state is active
    void MoveUpdate()
    {
        if (Vector3.Distance(transform.position, navAgent.destination) <= 0.1f)
            SetState(UnitState.Idle);
    }

    // called every frame the 'MoveToResource' state is active
    void MoveToResourceUpdate()
    {
        if (curResourceSource == null)
        {
            SetState(UnitState.Idle);
            return;
        }

        if (Vector3.Distance(transform.position, navAgent.destination) <= 1f)
        {
            SetState(UnitState.Gather);
        }
    }

    // called every frame the 'Gather' state is active
    void GatherUpdate()
    {
        if (curResourceSource == null)
        {
            FindNewResource();

            if (curResourceSource != null)
            {
                GatherResource(curResourceSource, curResourceSource.transform.position);
            }
            else
            {
                SetState(UnitState.RetreatToHQ);
            }
            
            return;
        }

        LookAt(curResourceSource.transform.position);

        if (Time.time - lastGatherTime > gatherRate)
        {
            lastGatherTime = Time.time;

            if (gatherCurrent < gatherCapacity)
            {
                curResourceSource.GatherResource(gatherAmount);

                carryType = curResourceSource.type;
                gatherCurrent += gatherAmount;
                

                //Debug.Log(gatherCurrent.ToString());

                //Debug.Log(curResourceSource);
            }
            else
            {
                SetState(UnitState.MoveToHQ);
            }
        }
    }

    // called every frame the 'MoveToEnemy' state is active
    void MoveToEnemyUpdate()
    {
        if (curEnemyUnitTarget == null)
        {
            SetState(UnitState.Idle);
            return;
        }

        if (Time.time - lastPathUpdateTime > pathUpdateRate)
        {
            lastPathUpdateTime = Time.time;
            navAgent.isStopped = false;
            navAgent.SetDestination(curEnemyUnitTarget.transform.position);
        }

        if (Vector3.Distance(transform.position, curEnemyUnitTarget.transform.position) <= attackDistance)
        {
            SetState(UnitState.AttackUnit);
        }
    }

    // called every frame the 'Attack' state is active
    void AttackUpdate()
    {
        // if our target is dead, go idle
        if (curEnemyUnitTarget == null)
        {
            SetState(UnitState.Idle);
            return;
        }

        // if we're still moving, stop
        if (!navAgent.isStopped)
        {
            navAgent.isStopped = true;
        }

        // attack every 'attackRate' seconds
        if (Time.time - lastAttackTime > attackRate)
        {
            lastAttackTime = Time.time;

            AudioManager.instance.PlayRandomGunSFX();
            curEnemyUnitTarget.TakeDamage(UnityEngine.Random.Range(minAttackDamage, maxAttackDamage + 1));
        }

        // look at the enemy
        LookAt(curEnemyUnitTarget.transform.position);

        // if we're too far away, move towards the enemy
        if (Vector3.Distance(transform.position, curEnemyUnitTarget.transform.position) > attackDistance)
        {
            SetState(UnitState.MoveToEnemy);
        }
    }

    // called every frame the 'MoveToEnemyBuilding' state is active
    void MoveToEnemyBuildingUpdate()
    {
        if (curEnemyBuildingTarget == null)
        {
            SetState(UnitState.Idle);
            return;
        }

        if (Time.time - lastPathUpdateTime > pathUpdateRate)
        {
            lastPathUpdateTime = Time.time;
            navAgent.isStopped = false;
            navAgent.SetDestination(curEnemyBuildingTarget.transform.position);
        }

        if ((Vector3.Distance(transform.position, curEnemyBuildingTarget.transform.position) - 4f) <= attackDistance)
        {
            SetState(UnitState.AttackBuilding);
        }
    }

    // called every frame the 'AttackBuilding' state is active
    void AttackBuildingUpdate()
    {
        // if our target is dead, go idle
        if (curEnemyBuildingTarget == null)
        {
            SetState(UnitState.Idle);
            return;
        }

        // if we're still moving, stop
        if (!navAgent.isStopped)
        {
            navAgent.isStopped = true;
        }

        // attack every 'attackRate' seconds
        if (Time.time - lastAttackTime > attackRate)
        {
            lastAttackTime = Time.time;

            AudioManager.instance.PlayRandomGunSFX();
            curEnemyBuildingTarget.TakeDamage(UnityEngine.Random.Range(minAttackDamage, maxAttackDamage + 1));
        }

        // look at the enemy
        LookAt(curEnemyBuildingTarget.transform.position);

        // if we're too far away, move towards the enemy's building
        if ((Vector3.Distance(transform.position, curEnemyBuildingTarget.transform.position) - 4f) > attackDistance)
        {
            SetState(UnitState.MoveToEnemyBuilding);
        }
    }

    void MoveToHQUpdate()
    {
        if (curResourceSource == null)
        {
            SetState(UnitState.Idle);
            return;
        }

        if (Time.time - lastPathUpdateTime > pathUpdateRate)
        {
            lastPathUpdateTime = Time.time;
            navAgent.isStopped = false;
            navAgent.SetDestination(player.startPosition.position);
        }

        //Debug.Log(Vector3.Distance(transform.position, player.startPosition.position));

        if (Vector3.Distance(transform.position, player.startPosition.position) <= 4f)
            SetState(UnitState.StoreAtHQ);
    }

    void StoreAtHQUpdate()
    {
        if (curResourceSource == null && gatherCurrent <= 0)
        {
            SetState(UnitState.Idle);
            return;
        }

        LookAt(player.startPosition.position);

        if (gatherCurrent > 0)
        {
            player.GainResource(carryType, gatherCurrent);
            gatherCurrent = 0;
        }
        else if (curResourceSource != null)
        {
            navAgent.destination = curResourceSource.transform.position;
            SetState(UnitState.MoveToResource);
        }
    }

    void RetreatToHQ()
    {
        if (Time.time - lastPathUpdateTime > pathUpdateRate)
        {
            lastPathUpdateTime = Time.time;
            navAgent.isStopped = false;
            navAgent.SetDestination(player.startPosition.position);
        }

        if (Vector3.Distance(transform.position, player.startPosition.position) <= 4f)
            SetState(UnitState.StoreAtHQ);
    }

    // called when an enemy unit attacks us
    public void TakeDamage(int damage)
    {
        curHP -= damage;

        if (curHP <= 0)
        {
            curHP = 0;
            Die();
        }

        // update the health bar
        healthBar.UpdateHealthBar(curHP, maxHP);
    }

    // called when our health reaches zero
    void Die()
    {
        player.units.Remove(this);

        InfoManager.instance.ClearAllInfo();

        GameManager.instance.UnitDeathCheck();

        Destroy(gameObject);
    }

    // move the units to a specific position
    public void MoveToPosition(Vector3 pos)
    {        
        navAgent.SetDestination(pos);

        navAgent.isStopped = false;

        SetState(UnitState.Move);
    }

    // move to a resource and begin to gather it
    public void GatherResource(ResourceSource resource, Vector3 pos)
    {
        curResourceSource = resource;

        if (curResourceSource.type != carryType)
        {
            carryType = curResourceSource.type;
            gatherCurrent = 0;
        }

        SetState(UnitState.MoveToResource);       

        navAgent.isStopped = false;
        navAgent.SetDestination(pos);
    }

    // move to an enemy unit and attack them
    public void AttackUnit(Unit target)
    {
        curEnemyUnitTarget = target;
        SetState(UnitState.MoveToEnemy);
    }

    // move to an enemy building and attack them
    public void AttackBuilding(Building target)
    {
        curEnemyBuildingTarget = target;
        SetState(UnitState.MoveToEnemyBuilding);
    }

    // toggles green selection ring around our feet
    public void ToggleSelectionVisual(bool selected)
    {
        if (greenSelectionVisual != null)
            greenSelectionVisual.SetActive(selected);
    }

    // rotate to face the given position
    void LookAt(Vector3 pos)
    {
        Vector3 dir = (pos - transform.position).normalized;
        float angle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(0, angle, 0);
    }

    void FindNewResource()
    {
        curResourceSource = player.GetClosestResource(transform.position, curResourceSource.type);
    }
}
