﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : MonoBehaviour
{
    public Faction faction;

    public bool isMe;

    private ResourceSource[] resources;

    [Header("Units")]
    public List<Unit> units = new List<Unit>();// units that this player currently has

    [Header("Buildings")]
    public List<Building> buildings = new List<Building>();// buildings that this player currently has

    [Header("Resources")]
    public int food;
    public int wood;
    public int stone;

    public int newResourceRange;

    [Header("Components")]
    public GameObject unitPrefab;
    public Transform startPosition;

    public Transform BuildingSpawnPos; //Temporary position to build a building

    public Transform unitsParent;
    public Transform buildingsParent;

    public Color accentColor;

    // events
    [System.Serializable]
    public class UnitCreatedEvent : UnityEvent<Unit> { }
    public UnitCreatedEvent onUnitCreated;

    public readonly int unitCost = 50;

    public Player player;
    public PlayerAI playerAI;

    void Awake()
    {
        //if (isMe)
        //{
        //    player = this;
        //}
    }

    void Start()
    {
        resources = FindObjectsOfType<ResourceSource>();

        if (isMe)
        {
            GameUI.instance.UpdateUnitCountText(units.Count);
            GameUI.instance.UpdateFoodText(food);

            CameraController.instance.FocusOnPosition(startPosition.position);
        }
        else
        {
            playerAI = GetComponent<PlayerAI>();
        }

        //CameraController.instance.FocusOnPosition(startPosition.position);
        
        food += unitCost;
        CreateFirstUnit();
    }

    // called when a unit gathers a certain resource
    public void GainResource(ResourceType resourceType, int amount)
    {
        switch(resourceType)
        {
            case ResourceType.Food:
            {
                food += amount;

                if (isMe)
                    GameUI.instance.UpdateFoodText(food);

                break;
            }

            case ResourceType.Wood:
            {
                wood += amount;

                if (isMe)
                    GameUI.instance.UpdateWoodText(wood);

                break;
            }
        }
    }

    // create a new unit for the player
    public void CreateFirstUnit()
    {
        if(food - unitCost < 0)
        {
            return;
        }

        GameObject unitObj = Instantiate(unitPrefab, startPosition.position, Quaternion.identity, transform);
        unitObj.transform.parent = unitsParent.transform;

        if (!isMe) // if I am an AI player
        {
            unitObj.AddComponent<UnitAI>();
        }

        Unit unit = unitObj.GetComponent<Unit>();

        units.Add(unit);
        unit.player = this; //set a unit's faction to be belong to this player
        food -= unitCost;

        //if(onUnitCreated != null)
        //{
        //onUnitCreated.Invoke(unit);
        //}

        if (isMe)
        {
            GameUI.instance.UpdateUnitCountText(units.Count);
            GameUI.instance.UpdateFoodText(food);
        }
        else //is AI
        {
            playerAI.OnUnitCreated(unit);
        }

    }

    // is this my unit?
    public bool IsMyUnit(Unit unit)
    {
        return units.Contains(unit);
    }

    // is this my building?
    public bool IsMyBuilding(Building building)
    {
        return buildings.Contains(building);
    }

    // gets the closest resource to the position (random between nearest 3 for some variance)
    public ResourceSource GetClosestResource(Vector3 pos, ResourceType rType)
    {
        //Debug.Log("Get Closest Resource");

        ResourceSource[] closest = new ResourceSource[3];
        float[] closestDist = new float[3];

        foreach (ResourceSource resource in resources)
        {
            if (resource == null)
                continue;

            if (resource.type == rType)
            {

                float dist = Vector3.Distance(pos, resource.transform.position);

                Debug.Log(resource + " " + dist);

                if (dist <= newResourceRange)
                {
                    for (int x = 0; x < closest.Length; x++)
                    {
                        if (closest[x] == null)
                        {
                            closest[x] = resource;
                            closestDist[x] = dist;
                            break;
                        }
                        else if (dist < closestDist[x])
                        {
                            closest[x] = resource;
                            closestDist[x] = dist;
                            break;
                        }

                    }
                }
            }
        }

        return closest[Random.Range(0, closest.Length)];
    }
}
