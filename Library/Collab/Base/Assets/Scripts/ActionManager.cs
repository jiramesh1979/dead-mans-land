﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;


public class ActionManager : MonoBehaviour
{
    public static ActionManager instance;

    public Button[] buttonUnitPic;
    public Button[] buttonBuildingPic;

    public Building curBuilding;

    public Unit curUnit;

    public ActionManager()
    {
        instance = this;
    }

    public void ShowCreateUnitButtons(Building building)
    {
        ClearAllInfo();

        curBuilding = building;

        if (curBuilding.isFunctional)
        {
            for (int i = 0; i < curBuilding.unitList.Length; i++)
            {
                buttonUnitPic[i].gameObject.SetActive(true);

                Unit unit = curBuilding.unitList[i].GetComponent<Unit>();

                buttonUnitPic[i].image.sprite = unit.unitPic;
            }
        }
    }

    public void HideCreateUnitButtons()
    {
        for (int i = 0; i < buttonUnitPic.Length; i++)
        {
            buttonUnitPic[i].gameObject.SetActive(false);
        }
    }

    public void ShowCreateBuildingButtons(Unit unit)
    {
        ClearAllInfo();

        curUnit = unit;

        if (curUnit.isBuilder)
        {
            for (int i = 0; i < curUnit.builder.buildingList.Length; i++)
            {
                buttonBuildingPic[i].gameObject.SetActive(true);

                Building building = curUnit.builder.buildingList[i].GetComponent<Building>();

                buttonBuildingPic[i].image.sprite = building.buildingPic;
            }
        }
    }

    public void HideCreateBuildingButtons()
    {
        for (int i = 0; i < buttonBuildingPic.Length; i++)
        {
            buttonBuildingPic[i].gameObject.SetActive(false);
        }
    }

    public void ClearAllInfo()
    {
        HideCreateUnitButtons();
        HideCreateBuildingButtons();
    }

    public void CreateUnitButton(int n)
    {
        //Debug.Log("Create " + n);

        curBuilding.CreateNewUnit(n);
    }

    public void CreateBuildingButton(int n)
    {
        //Debug.Log("Create Building " + n);

        curUnit.builder.ToCreateNewBuilding(n);
    }
}
