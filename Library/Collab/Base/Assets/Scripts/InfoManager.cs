﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InfoManager : MonoBehaviour
{
    public static InfoManager instance;

    public Image unitPic;

    public Image HPIcon;

    public TextMeshProUGUI Name, HP, Def, Attk, Rsrc;

    public InfoManager()
    {
        instance = this;
    }

    public void SetPic(Sprite pic)
    {
        //Unit Pic
        unitPic.sprite = pic;
        unitPic.color = Color.white;

        //HPIcon
        HPIcon.color = Color.white;
    }

    public void ClearPic()
    {
        //Unit Pic
        unitPic.color = Color.clear;

        //HPIcon
        HPIcon.color = Color.clear;
    }

    public void SetName(string name)
    {
        Name.text = name;
    }

    public void SetHP(string HPText)
    {
        HP.text = HPText;
    }

    public void ClearLines()
    {
        SetName("");
        SetHP("");
    }

    public void ShowUnitInfo(Unit unit)
    {
        SetPic(unit.unitPic);
        SetName(unit.unitName);
        SetHP(unit.curHp + "/" + unit.maxHp);
    }


    public void ClearUnitInfo()
    {
        ClearPic();
        ClearLines();
    }

    // Start is called before the first frame update
    void Start()
    {
        ClearLines();
        ClearPic();
    }

}
