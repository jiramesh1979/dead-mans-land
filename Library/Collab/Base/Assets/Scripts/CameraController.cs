﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public float moveSpeed;
    public float zoomSpeed;

    public float minZoomDist;
    public float maxZoomDist;

    private Camera cam;

    public Transform Corner1, Corner2;

    public static CameraController instance;

    private void Awake()
    {
        instance = this;
        cam = Camera.main;
    }

    void Update()
    {
        Move();
        Zoom();
    }

    void Move()
    {
        float xInput = Input.GetAxis("Horizontal");
        float zInput = Input.GetAxis("Vertical");

        Vector3 dir = transform.forward * zInput + transform.right * xInput;

        if (transform.position.x < Corner1.position.x)
            return;

        if (transform.position.z < Corner1.position.z)
            return;

        if (transform.position.x > Corner2.position.x)
            return;

        if (transform.position.z > Corner2.position.z)
            return;

        transform.position += dir * moveSpeed * Time.deltaTime;

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, Corner1.position.x, Corner2.position.x),
                                    transform.position.y,
                                    Mathf.Clamp(transform.position.z, Corner1.position.z, Corner2.position.z));
    }

    void Zoom()
    {
        float scrollInput = Input.GetAxis("Mouse ScrollWheel");
        float dist = Vector3.Distance(transform.position, cam.transform.position);
        //Debug.Log("Map = " + transform.position.ToString());
        //Debug.Log("Cam = " + cam.transform.position.ToString());
        //Debug.Log("Dist = " + dist.ToString());

        //Debug.Log("Scroll = " + scrollInput.ToString());

        if (dist < minZoomDist && scrollInput > 0.0f)
        {
            //Debug.Log("Too Close");
            return;
        }
        else if (dist > maxZoomDist && scrollInput < 0.0f)
        {
            //Debug.Log("Too Far");
            return;
        }

        cam.transform.position += cam.transform.forward * scrollInput * zoomSpeed;
    }

    public void FocusOnPosition(Vector3 pos)
    {
        transform.position = pos;
    }



}
