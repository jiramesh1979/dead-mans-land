﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPlayer : MonoBehaviour
{
    public float checkRate = 1.0f;
    public float nearbyEnemyAttackRange = 30f;

    public LayerMask unitLayerMask;

    Unit unit;

    void Awake()
    {
        unit = GetComponent<Unit>();
    }

    void Start()
    {
        InvokeRepeating("Check", 0.0f, checkRate);
        unitLayerMask = LayerMask.GetMask("Unit");
    }

    void Check()
    {
        if (!unit.isWorker)// if a unit is not a worker
        {
            // check if we have nearby enemies - if so, attack them
            if (unit.state != UnitState.AttackUnit && unit.state != UnitState.MoveToEnemy)
            {
                Unit potentialEnemy = CheckForNearbyEnemies();

                if (potentialEnemy != null)
                    unit.AttackUnit(potentialEnemy);
            }
        }
    }

    // checks for nearby enemies with a sphere cast
    Unit CheckForNearbyEnemies()
    {
        RaycastHit[] hits = Physics.SphereCastAll(transform.position, nearbyEnemyAttackRange, Vector3.up, unitLayerMask);

        GameObject closest = null;
        float closestDist = 0.0f;

        for (int x = 0; x < hits.Length; x++)
        {
            // skip if this is us
            if (hits[x].collider.gameObject == gameObject)
                continue;

            // is this a team mate?
            if (unit.player.IsMyUnit(hits[x].collider.GetComponent<Unit>()))
                continue;

            // if there is not closest enemy or the distance is less than the closest distance it currently has
            if (!closest || Vector3.Distance(transform.position, hits[x].transform.position) < closestDist)
            {
                closest = hits[x].collider.gameObject;
                closestDist = Vector3.Distance(transform.position, hits[x].transform.position);
            }
        }

        Debug.Log(closest + ", " + closestDist.ToString());

        if (closest != null)
            return closest.GetComponent<Unit>();
        else
            return null;
    }
}
