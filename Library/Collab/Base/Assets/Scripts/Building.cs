﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    [Header("Stats")]
    public string buildingName;
    public Sprite buildingPic;

    public bool isHQ;
    public bool isFunctional;

    public int curHP;
    public int maxHP;

    public int increment = 1;

    public int cost;

    [Header("Components")]
    public Player player;
    public GameObject SelectionVisual; //Selection Ring

    public UnitHealthBar healthBar;

    public GameObject fire;
    public GameObject smoke;

    public Transform unitSpawnPos;
    public Transform unitRallyPos;

    public GameObject[] unitList; //Units that this building can recruit

    public List<GameObject> recruitList = new List<GameObject>();

    public float timer = 0f;
    public float waitTime = 0.5f;

    void Start()
    {
        
    }

    void Update()
    {
        if (isFunctional && recruitList.Count > 0)
        {
            //Debug.Log(gameObject);

            timer += Time.deltaTime;

            //Debug.Log(timer);

            if (timer >= waitTime)
            {
                timer = 0;
                InfoManager.instance.ShowProgressMode(recruitList);

                if (InfoManager.instance.progressBar.CheckComplete())
                {
                    CreateUnitCompleted();
                }
            }
        }
    }

    // toggles green selection ring around resource
    public void ToggleSelectionVisual(bool selected)
    {
        if (SelectionVisual != null)
            SelectionVisual.SetActive(selected);
    }

    public void ToCreateUnit(int i)
    {
        if(player.food - player.unitCost < 0)
        {
            return;
        }

        player.food -= player.unitCost;

        if (player.isMe)
        {
            GameUI.instance.UpdateUnitCountText(player.units.Count);
            GameUI.instance.UpdateFoodText(player.food);
        }

        recruitList.Add(unitList[i]);

        CreateUnitProgress(recruitList);
    }

    void CreateUnitProgress(List<GameObject> list)
    {
        InfoManager.instance.ShowProgressMode(list);
    }

    public void CreateUnitCompleted()
    {
        GameObject unitObj = Instantiate(recruitList[0], unitSpawnPos.position, transform.rotation, transform);

        unitObj.transform.parent = player.unitsParent.transform;

        Unit unit = unitObj.GetComponent<Unit>();

        player.units.Add(unit);
        unit.player = this.player; //set a unit's faction to be belong to this player

        recruitList.RemoveAt(0);

        if (recruitList.Count == 0)
        {
            InfoManager.instance.HideProgressMode();
        }
        else
        {
            InfoManager.instance.ClearRecruitList();
            InfoManager.instance.ShowProgressMode(recruitList);
        }

        unit.MoveToPosition(unitRallyPos.position);
    }

    // called when an enemy unit attacks us
    public void TakeDamage(int damage)
    {
        curHP -= damage;

        if (curHP <= 0)
        {
            curHP = 0;
            Die();
        }

        fire.SetActive(true);
        smoke.SetActive(true);

        // update the health bar
        //healthBar.UpdateHealthBar(curHP, maxHP);
    }

    // called when our health reaches zero
    void Die()
    {
        player.buildings.Remove(this);

        InfoManager.instance.ClearAllInfo();

        GameManager.instance.UnitDeathCheck();

        Destroy(gameObject);
    }
}
